Demo project for GraphQL Yoga and Prisma

Prerequisite:
NodeJS, Prisma and docker installed

1 - In the project root, run cd prisma and docker-compose up -d
2 - Go back to the root folder and run npm i
3 - Run npm run start

Note: Prisma API on port 4466
GraphQL playground on port 4000
