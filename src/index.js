import { prisma } from './generated/prisma-client';
import { GraphQLServer, PubSub } from 'graphql-yoga';
import { resolvers } from './resolvers';

const pubsub = new PubSub();

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: request => {
    return {
      request,
      prisma
    };
  }
});

server.start(() => {
  console.log('Server running on port 4000');
});
