import bcrypt from 'bcryptjs';
import generateToken from '../utils/generateToken';
import getUserId from '../utils/getUserId';
import hashPassword from '../utils/hashPassword';

const Mutation = {
  createUser: async (parent, args, { prisma }, info) => {
    const emailTaken = await prisma.$exists.user({ email: args.data.email });

    if (emailTaken) {
      throw new Error('Email Taken');
    }

    const password = await hashPassword(args.data.password);
    const user = await prisma.createUser({
      ...args.data,
      password
    });

    return {
      user,
      token: generateToken(user.id)
    };
  },
  login: async (parent, args, { prisma }, info) => {
    const userExists = await prisma.$exists.user({ email: args.data.email });

    if (!userExists) {
      throw new Error('User not found');
    }

    const user = await prisma.user({ email: args.data.email });

    if (!user) {
      throw new Error('Unable to login');
    }

    const isMatch = await bcrypt.compare(args.data.password, user.password);

    if (!isMatch) {
      throw new Error('Password not much');
    }

    return {
      user,
      token: generateToken(user.id)
    };
  },
  deleteUser: (parent, args, { prisma, request }, info) => {
    const userId = getUserId(request);

    return prisma.deleteUser({ id: userId });
  },
  updateUser: async (parent, args, { prisma, request }, info) => {
    const userId = getUserId(request);

    if (typeof args.data.password === 'string') {
      args.data.password = await hashPassword(args.data.password);
    }

    return prisma.updateUser({
      where: {
        id: userId
      },
      data: args.data
    });
  },
  createPost: (parent, args, { prisma, request }, info) => {
    const userId = getUserId(request);

    return prisma.createPost({
      title: args.data.title,
      body: args.data.body,
      published: args.data.published,
      author: {
        connect: {
          id: userId
        }
      }
    });
  },
  deletePost: async (parent, args, { prisma, request }, info) => {
    const userId = getUserId(request);
    const postExists = await prisma.$exists.post({
      id: args.id,
      author: {
        id: userId
      }
    });

    if (!postExists) {
      throw new Error('Unable to delete post');
    }

    return prisma.deletePost({ id: args.id });
  },
  updatePost: async (parent, args, { prisma, request }, info) => {
    const userId = getUserId(request);
    const postExists = await prisma.$exists.post({
      id: args.id,
      author: {
        id: userId
      }
    });
    const isPublished = await prisma.$exists.post({
      id: args.id,
      published: true
    });

    if (!postExists) {
      throw new Error('Unable to update post');
    }

    if (isPublished && args.data.published === false) {
      await prisma.deleteManyComments({
        post: {
          id: args.id
        }
      });
    }

    return prisma.updatePost({
      where: {
        id: args.id
      },
      data: args.data
    });
  },
  createComment: async (parent, args, { prisma, request }, info) => {
    const userId = getUserId(request);
    const postExists = await prisma.$exists.post({
      id: args.data.post,
      published: true
    });

    if (!postExists) {
      throw new Error('Enable to find post');
    }

    return prisma.createComment({
      text: args.data.text,
      author: {
        connect: {
          id: userId
        }
      },
      post: {
        connect: {
          id: args.data.post
        }
      }
    });
  },
  deleteComment: async (parent, args, { prisma, request }, info) => {
    const userId = getUserId(request);
    const commentExists = await prisma.$exists.comment({
      id: args.id,
      author: {
        id: userId
      }
    });

    if (!commentExists) {
      throw new Error('Unable to delete comment');
    }

    return prisma.deleteComment({
      id: args.id
    });
  },
  updateComment: async (parent, args, { prisma, request }, info) => {
    const userId = getUserId(request);
    const commentExists = await prisma.$exists.comment({
      id: args.id,
      author: {
        id: userId
      }
    });

    if (!commentExists) {
      throw new Error('Unable to update comment');
    }

    return prisma.updateComment({
      where: {
        id: args.id
      },
      data: args.data
    });
  }
};

export { Mutation as default };
