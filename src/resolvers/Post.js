const Post = {
  author: (parent, args, { prisma }, info) => {
    return prisma
      .post({
        id: parent.id
      })
      .author();
  },
  comments: (parent, args, { prisma }, info) => {
    return prisma
      .post({
        id: parent.id
      })
      .comments();
  }
};

export { Post as default };
