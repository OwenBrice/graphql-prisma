import getUserId from '../utils/getUserId';

const Subscription = {
  comment: {
    subscribe: (parent, { postId }, { prisma }, info) => {
      return prisma.$subscribe.comment({
        node: {
          post: {
            id: postId
          }
        }
      });
    },
    resolve: payload => {
      return payload;
    }
  },
  post: {
    subscribe: (parent, args, { prisma }, info) => {
      return prisma.$subscribe.post({
        node: {
          published: true
        }
      });
    },
    resolve: payload => {
      return payload;
    }
  },
  myPost: {
    subscribe: (parent, args, { prisma, request }, info) => {
      const userId = getUserId(request);

      return prisma.$subscribe.post({
        node: {
          author: {
            id: userId
          }
        }
      });
    },
    resolve: payload => {
      return payload;
    }
  }
};

export { Subscription as default };
