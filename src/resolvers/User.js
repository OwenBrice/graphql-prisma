import getUserId from '../utils/getUserId';

const User = {
  email: (parent, args, { request }, info) => {
    const userId = getUserId(request, false);

    if (userId && userId === parent.id) {
      return parent.email;
    } else {
      return null;
    }
  },
  // # For older version
  // email: {
  //   fragment: 'fragment userId on User { id }',
  //   resolve: (parent, args, { request }, info) => {
  //     const userId = getUserId(request, false);

  //     if (userId && userId === parent.id) {
  //       return parent.email;
  //     } else {
  //       return null;
  //     }
  //   }
  // },
  posts: (parent, args, { prisma }, info) => {
    return prisma
      .user({
        id: parent.id
      })
      .posts({
        where: {
          published: true
        }
      });
  },
  comments: (parent, args, { prisma }, info) => {
    return prisma
      .user({
        id: parent.id
      })
      .comments();
  }
};

export { User as default };
