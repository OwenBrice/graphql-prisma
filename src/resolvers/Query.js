import getUserId from '../utils/getUserId';

const Query = {
  users: (parent, args, { prisma }, info) => {
    const opArgs = {
      first: args.first,
      skip: args.skip,
      after: args.after,
      orderBy: args.orderBy
    };

    if (args.query) {
      opArgs.where = {
        OR: [
          {
            name_contains: args.query
          }
        ]
      };
    }

    return prisma.users(opArgs);
  },
  posts: (parent, args, { prisma }, info) => {
    const opArgs = {
      first: args.first,
      skip: args.skip,
      after: args.after,
      orderBy: args.orderBy,
      where: {
        published: true
      }
    };

    if (args.query) {
      opArgs.where.OR = [
        {
          title_contains: args.query
        },
        {
          body_contains: args.query
        }
      ];
    }

    return prisma.posts(opArgs);
  },
  myPosts: async (parent, args, { prisma, request }, info) => {
    const userId = getUserId(request);

    const opArgs = {
      first: args.first,
      skip: args.skip,
      after: args.after,
      orderBy: args.orderBy,
      where: {
        author: {
          id: userId
        }
      }
    };

    if (args.query) {
      opArgs.where.OR = [
        {
          title_contains: args.query
        },
        {
          body_contains: args.query
        }
      ];
    }

    return prisma.posts(opArgs);
  },
  comments: (parent, args, { prisma }, info) => {
    const opArgs = {
      first: args.first,
      skip: args.skip,
      after: args.after,
      orderBy: args.orderBy
    };

    return prisma.comments(opArgs);
  },
  me: (parent, args, { prisma, request }, info) => {
    const userId = getUserId(request);

    return prisma.user({
      id: userId
    });
  },
  post: async (parent, args, { prisma, request }, info) => {
    const userId = getUserId(request, false);

    const posts = await prisma.posts({
      where: {
        id: args.id,
        OR: [
          {
            published: true
          },
          {
            author: {
              id: userId
            }
          }
        ]
      }
    });

    if (posts.length === 0) {
      throw new Error('Post not found');
    }

    return posts[0];
  }
};

export { Query as default };
